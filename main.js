// 1. Цей механізм дозволяє створювати ієрархії об'єктів та використовувати спільні властивості і методи, що спрощує роботу з кодом і сприяє його перевикористанню.
// 2. super() використовується для виклику конструктора батьківського класу.



class Employee {
    constructor(name, age, salary) {
        this._name = name;
        this._age = age;
        this._salary = salary;
    }

    set setName(value) {
        this._name = value;
    }

    get getName() {
        return this._name
    }

    set setAge(value) {
        this._age = value;
    }

    get getAge() {
        return this._age
    }

    set setSalary(value) {
        this._salary = value;
    }

    get getSalary() {
        return this._salary
    }
}

class Programmer extends Employee {
    constructor(name, age, salary, lang) {
        super(name, age, salary);
        this._lang = lang;
    }

    get getSalary() {
        return this._salary * 3;
    }

    set setLang(value) {
        this._lang = value
    }

    get getLang() {
        return this._lang.join(", ")
    }
}

const user1 = new Programmer("John", 20, 4000, ["C#", "JS", "TS"])
const user2 = new Programmer("Alexis", 19, 3000, ["C++"])
const user3 = new Programmer("Tony", 23, 5000, ["C#", "Java"])

console.log(user1);
console.log(user2);
console.log(user3);
